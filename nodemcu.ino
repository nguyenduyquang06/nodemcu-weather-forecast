#include <ESP8266WiFi.h>
#include <PubSubClient.h>
#include <TinyGPS++.h>
#include <SoftwareSerial.h>
// Cập nhật thông tin
// Thông tin về wifi


#include "DHT.h"
#define ssid "INTERNET-NAM&NGUYET"
#define password "bachkhoamottinhyeu"
//#define ssid "Mi51"
//#define password "emyeuanh"
// Thông tin về MQTT Broker
#define mqtt_server "m10.cloudmqtt.com"
#define mqtt_topic_pub "data"
#define mqtt_topic_sub "data"
#define mqtt_user "pszdadvc"
#define mqtt_pwd "eaNOhD6APiQW"

static const int RXPin = 12, TXPin = 13;
static const uint32_t GPSBaud = 9600;
const uint16_t mqtt_port = 12972 ; //Port của CloudMQTT
WiFiClient espClient;
PubSubClient client(espClient);
long lastMsg = 0;
long lastGPS = 0;
char msg[50];
int value = 0;
long now = 0;
#define DHTPIN D4      // Chân DATA nối với chân D4


#define DHTTYPE DHT11   // DHT 11


// The TinyGPS++ object
TinyGPSPlus gps;

// The serial connection to the GPS device
SoftwareSerial ss(RXPin, TXPin);



// Khởi tạo cảm biến
DHT dht(DHTPIN, DHTTYPE);
String gps_data;

void setup() {
  // Khởi tạo cổng serial baud 115200
  Serial.begin(115200);
  ss.begin(GPSBaud);
  Serial.println("DHTxx test!");
  setup_wifi();
  client.setServer(mqtt_server, mqtt_port);
  client.setCallback(callback);
  // Bắt đầu đọc dữ liệu
  dht.begin();
}
void setup_wifi() {
  delay(10);
  Serial.println();
  Serial.print("Connecting to ");
  Serial.println(ssid);
  WiFi.begin(ssid, password);
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }
  Serial.println("");
  Serial.println("WiFi connected");
  Serial.println("IP address: ");
  Serial.println(WiFi.localIP());
}

void callback(char* topic, byte* payload, unsigned int length) {
  Serial.print("Message arrived [");
  Serial.print(topic);
  Serial.print("] ");
  for (int i = 0; i < length; i++) {
    Serial.print((char)payload[i]);
  }
  Serial.println();
}



// Hàm reconnect thực hiện kết nối lại khi mất kết nối với MQTT Broker
void reconnect() {
  // Chờ tới khi kết nối
  while (!client.connected()) {
    Serial.print("Attempting MQTT connection...");
    // Thực hiện kết nối với mqtt user và pass
    if (client.connect("ESP8266Client", mqtt_user, mqtt_pwd)) {
      Serial.println("connected");
      // Khi kết nối sẽ publish thông báo
      //client.publish(mqtt_topic_pub, "ESP_reconnected");
      // ... và nhận lại thông tin này
      client.subscribe(mqtt_topic_sub);
    } else {
      Serial.print("failed, rc=");
      Serial.print(client.state());
      Serial.println(" try again in 5 seconds");
      // Đợi 5s
      delay(5000);
    }
  }
}
void loop() {
  now = millis();  
  while ((gps_data.length() < 1 || now - lastGPS > 1800000) && ss.available() > 0) {
    if (gps.encode(ss.read())) {
      delay(500);
      if (gps.location.isValid())
      {
        delay(500);
        lastGPS = now;
        double latitude = (gps.location.lat());
        double longitude = (gps.location.lng());
        gps_data = "";
        gps_data += (String(latitude, 6)) + "," + (String(longitude, 6)) + ",";
        Serial.print(gps_data);
        Serial.println(now);
        delay(10000);
      }
      else
      {
        Serial.print("INVALID");
        delay(10000);
        return;
      }
    } else {      
      return;
    }    
  }
  if (gps_data.length() > 1) {
    delay(10000); 
    Serial.println(now);
  }
  // Kiểm tra kết nối
  if (!client.connected()) {
    reconnect();
  }  
  client.loop();
  if (now - lastMsg > 100000) {
    // Đợi chuyển đổi dữ liệu khoảng 2s
    float h = dht.readHumidity();
    delay(200);
    // Đọc giá trị nhiệt độ C (mặc định)
    float t = dht.readTemperature();
    delay(200);

    // Kiểm tra quá trình đọc thành công hay không
    if (isnan(h) || isnan(t)) {
      Serial.println("Failed to read from DHT sensor!");
      return;
    }
    lastMsg = now;
    if (gps_data.length() < 1) {
      Serial.println("Upload data failed");
    } else {
      String pubString = gps_data;
      pubString += String(dht.readHumidity());
      pubString += ",";
      pubString += String(dht.readTemperature());
      unsigned int len =  pubString.length() + 1;
      char temp[len];
      pubString.toCharArray(temp, len);
      Serial.println(pubString);
      client.publish(mqtt_topic_pub, temp);
    }
  }


  // IN thông tin ra màn hình
  //  Serial.print("Do am: ");
  //  Serial.print(h);
  //  Serial.print(" %\t");
  //  Serial.print("Nhiet do: ");
  //  Serial.print(t);
  //  Serial.println(" *C ");
}
